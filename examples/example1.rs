use tracing::info;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

fn main() {
    tracing_subscriber::registry()
        .with(
            fmt::layer()
                .pretty()
                .with_writer(std::io::stderr)
                .without_time()
                .with_file(false),
        )
        .with(EnvFilter::from_default_env())
        .init();

    info!("Some info");
}
