alias e := example

example EXAMPLE $RUST_LOG="info":
  #!/usr/bin/env bash
  set -Eeuo pipefail
  mkdir -p target
  touch target/log.log || true
  cargo run --example {{EXAMPLE}} 2>target/log.log

tail:
  #!/usr/bin/env bash
  set -Eeuo pipefail
  touch target/log.log || true
  tail -f target/log.log
