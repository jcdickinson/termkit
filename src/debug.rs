pub struct PrintableSeq<'a, T: AsRef<[u8]>>(pub &'a T);

impl<'a, T: AsRef<[u8]>> std::fmt::Debug for PrintableSeq<'a, T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let r = self.0.as_ref();
        write!(f, "b\"")?;
        for c in r {
            match c {
                0x7F..=0xFF | 0x00..=0x1F => write!(f, "\\{:#04x}", c)?,
                b'\"' => write!(f, "\\\"")?,
                _ => write!(f, "{}", *c as char)?,
            }
        }
        write!(f, "\"")
    }
}

pub struct PrintableU8<'a, T: Into<u8> + Clone>(pub &'a T);

impl<'a, T: Into<u8> + Clone> std::fmt::Debug for PrintableU8<'a, T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let r = Into::<u8>::into(self.0.clone());
        match r {
            0x7F..=0xFF | 0x00..=0x1F => write!(f, "{:#04x}", r),
            b'\'' => write!(f, "b'\\''"),
            _ => write!(f, "b'{}'", r as char),
        }
    }
}
