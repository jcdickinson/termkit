use std::{
    fmt::Debug,
    io::Read,
    ops::{Deref, Range},
};

use crate::debug::PrintableSeq;

const DEFAULT_SIZE: usize = 512;
const GROWTH_NUM: usize = 3;
const GROWTH_DEN: usize = 2;

pub struct Buffer {
    data: Box<[u8]>,
    available: Range<usize>,
    consumed: usize,
}

#[cfg(not(tarpaulin_include))]
impl Debug for Buffer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("Buffer").field(&PrintableSeq(self)).finish()
    }
}

impl Default for Buffer {
    fn default() -> Self {
        Self {
            data: vec![0u8; DEFAULT_SIZE].into_boxed_slice(),
            available: 0..0,
            consumed: 0,
        }
    }
}

impl Deref for Buffer {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        self.as_slice()
    }
}

impl AsRef<[u8]> for Buffer {
    fn as_ref(&self) -> &[u8] {
        self.as_slice()
    }
}

impl Buffer {
    #[cfg(test)]
    pub fn from_slice(value: impl AsRef<[u8]>) -> Self {
        let value = value.as_ref();
        let len = value.len();
        Buffer {
            data: value.to_vec().into_boxed_slice(),
            available: 0..len,
            consumed: 0,
        }
    }

    pub fn read_from<R>(&mut self, r: &mut R) -> std::io::Result<usize>
    where
        R: Read + ?Sized,
    {
        debug_assert_ne!(self.data.len() - self.available.end, 0);
        let l = r.read(&mut self.data[self.available.end..])?;
        self.available = self.available.start..(self.available.end + l);
        Ok(l)
    }

    pub fn push(&mut self, b: impl AsRef<[u8]>) {
        let b = b.as_ref();
        self.ensure_capacity(b.len());
        let fin = self.available.end + b.len();
        self.data[self.available.end..fin].copy_from_slice(b);
        self.available = self.available.start..fin;
    }

    pub fn consumed(&self) -> usize {
        self.consumed
    }

    #[inline]
    pub fn as_slice(&self) -> &[u8] {
        &self.data[self.available.clone()]
    }

    // We disable check_size in tests so that we witness the behavior under release,
    // which is what really matters.
    #[cfg(test)]
    fn check_size(&self, _: usize) {}

    #[cfg(not(test))]
    fn check_size(&self, size: usize) {
        debug_assert!(size <= self.available.len());
    }

    pub fn consume(&mut self, size: usize) -> usize {
        if size == 0 {
            return self.available.len();
        }

        self.check_size(size);
        let size = size.min(self.available.len());
        self.consumed += size;
        self.available.start += size;
        let l = self.available.len();
        if l == 0 {
            self.available = 0..0;
        }
        l
    }

    pub fn ensure_capacity(&mut self, capacity: usize) {
        let required = self.available.end + capacity;
        if required <= self.data.len() {
            return;
        }

        self.data.copy_within(self.available.clone(), 0);
        self.available = 0..(self.available.len());

        let required = self.available.end + capacity;
        if required <= self.data.len() {
            return;
        }

        let mut to = self.data.len();
        while required > to {
            to = (to / GROWTH_DEN)
                .checked_mul(GROWTH_NUM)
                .expect("overflow occurred");
        }

        let mut data = vec![0u8; to].into_boxed_slice();
        data[self.available.clone()].copy_from_slice(&self.data[self.available.clone()]);

        self.data = data;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn growth_ignores_consumed() {
        let mut sut = Buffer::default();
        for i in 0..sut.data.len() {
            sut.data[i] = ((i * i) & 0xFF) as u8;
        }

        sut.ensure_capacity(DEFAULT_SIZE + 1);

        assert_eq!(sut.data.len(), (DEFAULT_SIZE * GROWTH_NUM) / GROWTH_DEN);

        for i in 0..sut.data.len() {
            assert_eq!(sut.data[i], 0);
        }
    }

    #[test]
    fn growth_skipped_if_compacted() {
        let mut sut = Buffer {
            available: 500..512,
            ..Buffer::default()
        };

        for i in 0..sut.data.len() {
            sut.data[i] = ((i * i) & 0xFF) as u8;
        }

        sut.ensure_capacity(20);

        assert_eq!(sut.data.len(), DEFAULT_SIZE);
        for i in 0..12 {
            let j = i + 500;
            assert_eq!(sut.data[i], ((j * j) & 0xFF) as u8);
        }
    }

    #[test]
    fn growth_copies_available() {
        let mut sut = Buffer::default();
        for i in 0..sut.data.len() {
            sut.data[i] = ((i * i) & 0xFF) as u8;
        }
        sut.available = 50..100;
        sut.ensure_capacity(DEFAULT_SIZE + 1);

        for i in 0..50 {
            let j = i + 50;
            assert_eq!(sut.data[i], ((j * j) & 0xFF) as u8);
        }
    }

    #[test]
    fn no_growth_if_large_enough() {
        let mut sut = Buffer::default();
        sut.ensure_capacity(10);
        assert_eq!(sut.data.len(), DEFAULT_SIZE);
    }

    #[test]
    fn no_growth_if_large_enough_2() {
        let mut sut = Buffer::default();
        sut.ensure_capacity(10);
        assert_eq!(sut.data.len(), DEFAULT_SIZE);
    }

    #[test]
    fn growth_with_bytes_available() {
        let mut sut = Buffer {
            available: 50..100,
            ..Default::default()
        };
        sut.ensure_capacity(DEFAULT_SIZE - 1);
        assert_eq!(sut.data.len(), (DEFAULT_SIZE * GROWTH_NUM) / GROWTH_DEN);
    }

    #[test]
    fn consume_deletes_bytes() {
        let mut sut = Buffer {
            available: 50..100,
            ..Default::default()
        };
        sut.consume(10);
        assert_eq!(sut.consumed(), 10);
        assert_eq!(sut.available, 60..100);
    }

    #[test]
    fn consume_capped() {
        let mut sut = Buffer {
            available: 50..100,
            ..Default::default()
        };
        sut.consume(60);
        assert_eq!(sut.consumed(), 50);
        assert_eq!(sut.available.len(), 0);
    }

    #[test]
    fn consume_nothing() {
        let mut sut = Buffer {
            available: 50..100,
            ..Default::default()
        };
        assert_eq!(sut.consume(0), 50);
    }

    #[test]
    fn read_from() {
        let mut sut = Buffer::default();
        assert_eq!(sut.read_from(&mut b"test".as_slice()).unwrap(), 4);
        sut.consume(3);
        assert_eq!(sut.read_from(&mut b"123".as_slice()).unwrap(), 3);
        assert_eq!(sut.deref(), b"t123");
    }

    #[test]
    fn from_slice() {
        let sut = Buffer::from_slice(b"1234");
        assert_eq!(sut.deref(), b"1234");
    }

    #[test]
    fn as_ref() {
        let sut = Buffer::from_slice(b"1234");
        assert_eq!(sut.as_ref(), b"1234");
    }

    #[test]
    fn push() {
        let mut sut = Buffer::from_slice(b"1234");
        sut.push(b"5678");
        assert_eq!(sut.as_ref(), b"12345678");
    }
}
