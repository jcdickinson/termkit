#![cfg_attr(coverage_nightly, feature(no_coverage))]

/// VT Parser
///
/// Provides a parser that loosely conforms to
/// [Paul Flo Williams' VT100 State Machine]. Several adjustments have been
/// made to ensure that it covers the entire V100 spec, instead of documented
/// function. This means that it should be able to handle any user-defined
/// extensions to the protocol.
///
/// [Paul Flo Williams' VT100 State Machine]: https://vt100.net/emu/dec_ansi_parser
pub mod vt;

pub use io::buffer::Buffer;

#[cfg(not(tarpaulin_include))]
mod debug;
mod io;
