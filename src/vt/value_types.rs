use thiserror::Error;

pub const PARAM_MAX: u16 = 9999;

#[derive(Debug, Error, Eq, PartialEq)]
#[error("invalid parameter value")]
pub struct ParamValueErr;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ParamValue(u16);

impl ParamValue {
    pub const fn from_u16_lossy(value: u16) -> Self {
        if value >= PARAM_MAX {
            Self(PARAM_MAX)
        } else {
            Self(value)
        }
    }

    pub const fn value(&self) -> u16 {
        self.0
    }
}

impl TryFrom<u16> for ParamValue {
    type Error = ParamValueErr;

    fn try_from(value: u16) -> Result<Self, Self::Error> {
        if value <= PARAM_MAX {
            Ok(ParamValue(value))
        } else {
            Err(ParamValueErr)
        }
    }
}

impl From<ParamValue> for u16 {
    fn from(value: ParamValue) -> Self {
        value.value()
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Command {
    /// Reserved values.
    Reserved(u8),

    /// `C0 - 00/00`
    /// NUL is used for media-fill or time-fill. NUL characters may be inserted into, or removed from, a data stream
    /// without affecting the information content of that stream, but such action may affect the information layout
    /// and/or the control of equipment.
    Null,
    /// `C0 - 00/01`
    /// SOH is used to indicate the beginning of a heading.
    StartOfHeading,
    /// `C0 - 00/02`
    /// STX is used to indicate the beginning of a text and the end of a heading.
    StartOfText,
    /// ETX is used to indicate the end of a text.
    /// `C0 - 00/03`
    EndOfText,
    /// `C0 - 00/04`
    /// EOT is used to indicate the conclusion of the transmission of one or more texts.
    EndOfTransmission,
    /// `C0 - 00/05`
    /// ENQ is transmitted by a sender as a request for a response from a receiver.
    Enquiry,
    /// `C0 - 00/06`
    /// ACK is transmitted by a receiver as an affirmative response to the sender.
    Acknowledge,
    /// `C0 - 00/07`
    /// BEL is used when there is a need to call for attention; it may control alarm or attention devices.
    Bell,
    /// `C0 - 00/08`
    /// BS causes the active data position to be moved one character position in the data component in the direction
    /// opposite to that of the implicit movement.
    ///
    /// The direction of the implicit movement depends on the parameter value of SELECT IMPLICIT MOVEMENT DIRECTION
    /// (SIMD).
    Backspace,
    /// `C0 - 00/09`
    /// HT causes the active presentation position to be moved to the following character tabulation stop in the
    /// presentation component.
    ///
    /// In addition, if that following character tabulation stop has been set by TABULATION ALIGN CENTRE (TAC),
    /// TABULATION ALIGN LEADING EDGE (TALE), TABULATION ALIGN TRAILING EDGE (TATE) or TABULATION CENTRED ON CHARACTER
    /// (TCC), HT indicates the beginning of a string of text which is to be positioned within a line according to the
    /// properties of that tabulation stop. The end of the string is indicated by the next occurrence of HT or CARRIAGE
    /// RETURN (CR) or NEXT LINE (NEL) in the data stream.
    CharacterTabulation,
    /// `C0 - 00/10`
    /// If the DEVICE COMPONENT SELECT MODE (DCSM) is set to PRESENTATION, LF causes the active presentation position
    /// to be moved to the corresponding character position of the following line in the presentation component.
    ///
    /// If the DEVICE COMPONENT SELECT MODE (DCSM) is set to DATA, LF causes the active data position to be moved to
    /// the corresponding character position of the following line in the data component.
    LineFeed,
    /// `C0 - 00/11`
    /// VT causes the active presentation position to be moved in the presentation component to the corresponding
    /// character position on the line at which the following line tabulation stop is set.
    LineTabulation,
    /// `C0 - 00/12`
    /// FF causes the active presentation position to be moved to the corresponding character position of the line at
    /// the page home position of the next form or page in the presentation component. The page home position is
    /// established by the parameter value of SET PAGE HOME (SPH).
    FormFeed,
    /// `C0 - 00/13`
    /// The effect of CR depends on the setting of the DEVICE COMPONENT SELECT MODE (DCSM) and on the parameter value
    /// of SELECT IMPLICIT MOVEMENT DIRECTION (SIMD).
    ///
    /// If the DEVICE COMPONENT SELECT MODE (DCSM) is set to PRESENTATION and with the parameter value of SIMD equal to
    /// 0, CR causes the active presentation position to be moved to the line home position of the same line in the
    /// presentation component. The line home position is established by the parameter value of SET LINE HOME (SLH).
    ///
    /// With a parameter value of SIMD equal to 1, CR causes the active presentation position to be moved to the line
    /// limit position of the same line in the presentation component. The line limit position is established by the
    /// parameter value of SET LINE LIMIT (SLL).
    ///
    /// If the DEVICE COMPONENT SELECT MODE (DCSM) is set to DATA and with a parameter value of SIMD equal to 0, CR
    /// causes the active data position to be moved to the line home position of the same line in the data component.
    /// The line home position is established by the parameter value of SET LINE HOME (SLH).
    ///
    /// With a parameter value of SIMD equal to 1, CR causes the active data position to be moved to the line limit
    /// position of the same line in the data component. The line limit position is established by the parameter value
    /// of SET LINE LIMIT (SLL).
    CarriageReturn,
    /// `C0 - 00/14`
    /// SO is used for code extension purposes. It causes the meanings of the bit combinations following it in the
    /// data stream to be changed.
    ShiftOut,
    /// `C0 - 00/15`
    /// SI is used for code extension purposes. It causes the meanings of the bit combinations following it in the
    /// data stream to be changed.
    ShiftIn,

    /// `C0 - 01/00`
    /// DLE is used exclusively to provide supplementary transmission control functions.
    DataLinkEscape,
    /// `C0 - 01/01`
    /// DC1 is primarily intended for turning on or starting an ancillary device. If it is not required for this
    /// purpose, it may be used to restore a device to the basic mode of operation (see also DC2 and DC3), or any
    /// other device control function not provided by other DCs.
    DeviceControlOne,
    /// `C0 - 01/02`
    /// DC2 is primarily intended for turning on or starting an ancillary device. If it is not required for this
    /// purpose, it may be used to set a device to a special mode of operation (in which case DC1 is used to restore
    /// the device to the basic mode), or for any other device control function not provided by other DCs.
    DeviceControlTwo,
    /// `C0 - 01/03`
    /// DC3 is primarily intended for turning off or stopping an ancillary device. This function may be a secondary
    /// level stop, for example wait, pause, stand-by or halt (in which case DC1 is used to restore normal operation).
    /// If it is not required for this purpose, it may be used for any other device control function not provided by
    /// other DCs.
    DeviceControlThree,
    /// `C0 - 01/04`
    /// DC4 is primarily intended for turning off, stopping or interrupting an ancillary device. If it is not required
    /// for this purpose, it may be used for any other device control function not provided by other DCs.
    DeviceControlFour,
    /// `C0 - 01/05`
    /// NAK is transmitted by a receiver as a negative response to the sender.
    NegativeAcknowledge,
    /// `C0 - 01/06`
    /// SYN is used by a synchronous transmission system in the absence of any other character (idle condition) to
    /// provide a signal from which synchronism may be achieved or retained between data terminal equipment.
    SynchronousIdle,
    /// `C0 - 01/07`
    /// ETB is used to indicate the end of a block of data where the data are divided into such blocks for transmission
    /// purposes.
    EndOfTransmissionBlock,
    // CAN Excluded
    /// `C0 - 01/09`
    /// EM is used to identify the physical end of a medium, or the end of the used portion of a medium, or the end of
    /// the wanted portion of data recorded on a medium.
    EndOfMedium,
    // SUB Excluded
    // ESC Excluded
    /// `C0 - 01/12`
    /// IS4 is used to separate and qualify data logically; its specific meaning has to be defined for each
    /// application.
    InformationSeparatorFour,
    /// `C0 - 01/13`
    /// IS3 is used to separate and qualify data logically; its specific meaning has to be defined for each
    /// application
    InformationSeparatorThree,
    /// `C0 - 01/14`
    /// IS2 is used to separate and qualify data logically; its specific meaning has to be defined for each
    /// application
    InformationSeparatorTwo,
    /// `C0 - 01/15`
    /// IS1 is used to separate and qualify data logically; its specific meaning has to be defined for each
    /// application.
    InformationSeparatorOne,

    // Reserved
    // Reserved
    /// `C1 - ESC 04/02 or 08/02`
    /// BPH is used to indicate a point where a line break may occur when text is formatted. BPH may occur between two
    /// graphic characters, either or both of which may be SPACE.
    BreakPermittedHere,
    /// `C1 - ESC 04/03 or 08/03`
    /// NBH is used to indicate a point where a line break shall not occur when text is formatted. NBH may occur
    /// between two graphic characters either or both of which may be SPACE.
    NoBreakHere,
    // Reserved
    /// `C1 - ESC 04/05 or 08/05`
    /// The effect of NEL depends on the setting of the DEVICE COMPONENT SELECT MODE (DCSM) and on the parameter value
    /// of SELECT IMPLICIT MOVEMENT DIRECTION (SIMD).
    ///
    /// If the DEVICE COMPONENT SELECT MODE (DCSM) is set to PRESENTATION and with a parameter value of SIMD equal to
    /// 0, NEL causes the active presentation position to be moved to the line home position of the following line in
    /// the presentation component. The line home position is established by the parameter value of SET LINE HOME
    /// (SLH).
    NextLine,
    /// `C1 - ESC 04/06 or 08/06`
    /// SSA is used to indicate that the active presentation position is the first of a string of character positions
    /// in the presentation component, the contents of which are eligible to be transmitted in the form of a data
    /// stream or transferred to an auxiliary input/output device.
    ///
    /// The end of this string is indicated by END OF SELECTED AREA (ESA). The string of characters actually
    /// transmitted or transferred depends on the setting of the GUARDED AREA TRANSFER MODE (GATM) and on any guarded
    /// areas established by DEFINE AREA QUALIFICATION (DAQ), or by START OF GUARDED AREA (SPA) and END OF GUARDED AREA
    /// (EPA).
    StartOfSelectedArea,
    /// `C1 - ESC 04/07 or 08/07`
    /// ESA is used to indicate that the active presentation position is the last of a string of character positions in
    /// the presentation component, the contents of which are eligible to be transmitted in the form of a data stream
    /// or transferred to an auxiliary input/output device. The beginning of this string is indicated by START OF
    /// SELECTED AREA (SSA).
    EndOfSelectedArea,
    /// `C1 - ESC 04/08 or 08/08`
    /// HTS causes a character tabulation stop to be set at the active presentation position in the presentation
    /// component.
    CharacterTabulationSet,
    /// `C1 - ESC 04/09 or 08/09`
    /// HTJ causes the contents of the active field (the field in the presentation component that contains the active
    /// presentation position) to be shifted forward so that it ends at the character position preceding the following
    /// character tabulation stop. The active presentation position is moved to that following character tabulation
    /// stop. The character positions which precede the beginning of the shifted string are put into the erased state.
    CharacterTabulationWithJustification,
    /// `C1 - ESC 04/10 or 08/10`
    /// VTS causes a line tabulation stop to be set at the active line (the line that contains the active presentation
    /// position).
    LineTabulationSet,
    /// `C1 - ESC 04/11 or 08/11`
    /// PLD causes the active presentation position to be moved in the presentation component to the corresponding
    /// position of an imaginary line with a partial offset in the direction of the line progression. This offset
    /// should be sufficient either to image following characters as subscripts until the first following occurrence of
    /// PARTIAL LINE BACKWARD (PLU) in the data stream, or, if preceding characters were imaged as superscripts, to
    /// restore imaging of following characters to the active line (the line that contains the active presentation
    /// position).
    PartialLineForward,
    /// `C1 - ESC 04/12 or 08/12`
    /// PLU causes the active presentation position to be moved in the presentation component to the corresponding
    /// position of an imaginary line with a partial offset in the direction opposite to that of the line progression.
    /// This offset should be sufficient either to image following characters as superscripts until the first following
    /// occurrence of PARTIAL LINE FORWARD (PLD) in the data stream, or, if preceding characters were imaged as
    /// subscripts, to restore imaging of following characters to the active line (the line that contains the active
    /// presentation position).
    PartialLineBackward,
    /// `C1 - ESC 04/13 or 08/13`
    /// If the DEVICE COMPONENT SELECT MODE (DCSM) is set to PRESENTATION, RI causes the active presentation position
    /// to be moved in the presentation component to the corresponding character position of the preceding line.
    ///
    /// If the DEVICE COMPONENT SELECT MODE (DCSM) is set to DATA, RI causes the active data position to be moved in
    /// the data component to the corresponding character position of the preceding line.
    ReverseLineFeed,
    /// `C1 - ESC 04/14 or 08/14`
    /// SS2 is used for code extension purposes. It causes the meanings of the bit combinations following it in the
    /// data stream to be changed.
    SingleShiftTwo,
    /// `C1 - ESC 04/15 or 08/15`
    /// SS3 is used for code extension purposes. It causes the meanings of the bit combinations following it in the
    /// data stream to be changed.
    SingleShiftThree,

    // DCS Excluded
    /// `C1 - ESC 05/01 or 09/01`
    /// PU1 is reserved for a function without standardized meaning for private use as required, subject to the prior
    /// agreement between the sender and the recipient of the data.
    PrivateUseOne,
    /// `C1 - ESC 05/02 or 09/02`
    /// PU2 is reserved for a function without standardized meaning for private use as required, subject to the prior
    /// agreement between the sender and the recipient of the data.
    PrivateUseTwo,
    /// `C1 - ESC 05/03 or 09/03`
    /// STS is used to establish the transmit state in the receiving device. In this state the transmission of data
    /// from the device is possible.
    SetTransmitState,
    /// `C1 - ESC 05/04 or 09/04`
    /// CCH is used to indicate that both the preceding graphic character in the data stream, (represented by one or
    /// more bit combinations) including SPACE, and the control function CCH itself are to be ignored for further
    /// interpretation of the data stream.
    CancelCharacter,
    /// `C1 - ESC 05/05 or 09/05`
    /// MW is used to set a message waiting indicator in the receiving device. An appropriate acknowledgement to the
    /// receipt of MW may be given by using DEVICE STATUS REPORT (DSR).
    MessageWaiting,
    /// `C1 - ESC 05/06 or 09/06`
    /// SPA is used to indicate that the active presentation position is the first of a string of character positions
    /// in the presentation component, the contents of which are protected against manual alteration, are guarded
    /// against transmission or transfer, depending on the setting of the GUARDED AREA TRANSFER MODE (GATM) and may be
    /// protected against erasure, depending on the setting of the ERASURE MODE (ERM). The end of this string is
    /// indicated by END OF GUARDED AREA (EPA).
    StartOfGuardedArea,
    /// `C1 - ESC 05/07 or 09/07`
    /// EPA is used to indicate that the active presentation position is the last of a string of character positions in
    /// the presentation component, the contents of which are protected against manual alteration, are guarded against
    /// transmission or transfer, depending on the setting of the GUARDED AREA TRANSFER MODE (GATM), and may be
    /// protected against erasure, depending on the setting of the ERASURE MODE (ERM). The beginning of this string is
    /// indicated by START OF GUARDED AREA (SPA).
    EndOfGuardedArea,
    // SOS Excluded
    // Reserved
    // SCI Excluded
    // CSI Excluded
    // ST Excluded
    // OSC Excluded
    // PM Excluded
    // APC Excluded
}

impl From<u8> for Command {
    fn from(value: u8) -> Self {
        use Command::*;
        match value {
            // C0
            0x00 => Null,
            0x01 => StartOfHeading,
            0x02 => StartOfText,
            0x03 => EndOfText,
            0x04 => EndOfTransmission,
            0x05 => Enquiry,
            0x06 => Acknowledge,
            0x07 => Bell,
            0x08 => Backspace,
            0x09 => CharacterTabulation,
            0x0A => LineFeed,
            0x0B => LineTabulation,
            0x0C => FormFeed,
            0x0D => CarriageReturn,
            0x0E => ShiftOut,
            0x0F => ShiftIn,
            0x10 => DataLinkEscape,
            0x11 => DeviceControlOne,
            0x12 => DeviceControlTwo,
            0x13 => DeviceControlThree,
            0x14 => DeviceControlFour,
            0x15 => NegativeAcknowledge,
            0x16 => SynchronousIdle,
            0x17 => EndOfTransmissionBlock,
            0x19 => EndOfMedium,
            0x1C => InformationSeparatorFour,
            0x1D => InformationSeparatorThree,
            0x1E => InformationSeparatorTwo,
            0x1F => InformationSeparatorOne,

            // C1
            0x42 | 0x82 => BreakPermittedHere,
            0x43 | 0x83 => NoBreakHere,
            0x45 | 0x85 => NextLine,
            0x46 | 0x86 => StartOfSelectedArea,
            0x47 | 0x87 => EndOfSelectedArea,
            0x48 | 0x88 => CharacterTabulationSet,
            0x49 | 0x89 => CharacterTabulationWithJustification,
            0x4A | 0x8A => LineTabulationSet,
            0x4B | 0x8B => PartialLineForward,
            0x4C | 0x8C => PartialLineBackward,
            0x4D | 0x8D => ReverseLineFeed,
            0x4E | 0x8E => SingleShiftTwo,
            0x4F | 0x8F => SingleShiftThree,
            0x51 | 0x91 => PrivateUseOne,
            0x52 | 0x92 => PrivateUseTwo,
            0x53 | 0x93 => SetTransmitState,
            0x54 | 0x94 => CancelCharacter,
            0x55 | 0x95 => MessageWaiting,
            0x56 | 0x96 => StartOfGuardedArea,
            0x57 | 0x97 => EndOfGuardedArea,

            other => Reserved(other),
        }
    }
}

impl From<Command> for u8 {
    fn from(value: Command) -> Self {
        use Command::*;
        match value {
            // C0
            Null => 0x00,
            StartOfHeading => 0x01,
            StartOfText => 0x02,
            EndOfText => 0x03,
            EndOfTransmission => 0x04,
            Enquiry => 0x05,
            Acknowledge => 0x06,
            Bell => 0x07,
            Backspace => 0x08,
            CharacterTabulation => 0x09,
            LineFeed => 0x0A,
            LineTabulation => 0x0B,
            FormFeed => 0x0C,
            CarriageReturn => 0x0D,
            ShiftOut => 0x0E,
            ShiftIn => 0x0F,
            DataLinkEscape => 0x10,
            DeviceControlOne => 0x11,
            DeviceControlTwo => 0x12,
            DeviceControlThree => 0x13,
            DeviceControlFour => 0x14,
            NegativeAcknowledge => 0x15,
            SynchronousIdle => 0x16,
            EndOfTransmissionBlock => 0x17,
            EndOfMedium => 0x19,
            InformationSeparatorFour => 0x1C,
            InformationSeparatorThree => 0x1D,
            InformationSeparatorTwo => 0x1E,
            InformationSeparatorOne => 0x1F,

            // C1
            BreakPermittedHere => 0x82,
            NoBreakHere => 0x83,
            NextLine => 0x85,
            StartOfSelectedArea => 0x86,
            EndOfSelectedArea => 0x87,
            CharacterTabulationSet => 0x88,
            CharacterTabulationWithJustification => 0x89,
            LineTabulationSet => 0x8A,
            PartialLineForward => 0x8B,
            PartialLineBackward => 0x8C,
            ReverseLineFeed => 0x8D,
            SingleShiftTwo => 0x8E,
            SingleShiftThree => 0x8F,
            PrivateUseOne => 0x91,
            PrivateUseTwo => 0x92,
            SetTransmitState => 0x93,
            CancelCharacter => 0x94,
            MessageWaiting => 0x95,
            StartOfGuardedArea => 0x96,
            EndOfGuardedArea => 0x97,

            Reserved(other) => other,
        }
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn param_value_try_from() {
        assert_eq!(ParamValue::try_from(0), Ok(ParamValue(0)));
        assert_eq!(ParamValue::try_from(999), Ok(ParamValue(999)));
        assert_eq!(ParamValue::try_from(9999), Ok(ParamValue(9999)));
        assert_eq!(ParamValue::try_from(10000), Err(ParamValueErr));
    }

    #[test]
    fn param_value_from_utf16_lossy() {
        assert_eq!(ParamValue::from_u16_lossy(0), ParamValue(0));
        assert_eq!(ParamValue::from_u16_lossy(999), ParamValue(999));
        assert_eq!(ParamValue::from_u16_lossy(9999), ParamValue(9999));
        assert_eq!(ParamValue::from_u16_lossy(10000), ParamValue(9999));
    }

    #[test]
    fn param_value_into() {
        assert_eq!(u16::from(ParamValue(0)), 0);
        assert_eq!(u16::from(ParamValue(999)), 999);
    }

    #[test]
    fn command_is_itself() {
        for i in 0..0xFFu8 {
            let command: Command = i.into();
            let ch: u8 = command.into();

            if (0x40..=0x5F).contains(&i) && (0x80..=0x9F).contains(&ch) {
                assert_eq!(ch - 0x40, i);
            } else {
                assert_eq!(ch, i);
            }
        }
    }
}
