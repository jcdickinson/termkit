// I'm pretty sure that things over 0x80 are identical to thing-0x80,
// except for the well-known escape codes (like 0x9C etc.).

use super::{ParamValue, PARAM_MAX};

#[derive(Debug, PartialEq, Eq)]
pub enum TokenType<'a> {
    None,
    Execute(u8),
    Print(&'a [u8]),
    Put(&'a [u8]),
    Param(ParamValue),
    Dispatch(u8),
    Collect(u8),
    Sci(u8),
    EscStart,
    CsiStart,
    DcsStart,
    OscStart,
    SosStart,
    PmStart,
    ApcStart,
    EndString,
}
use TokenType::*;

impl<'a> Default for TokenType<'a> {
    fn default() -> Self {
        Self::None
    }
}

#[derive(Debug, Default, PartialEq, Eq)]
pub struct Token<'a> {
    token: TokenType<'a>,
    consumed: usize,
    required: usize,
}

impl<'a> Token<'a> {
    pub fn token(&self) -> &TokenType<'a> {
        &self.token
    }

    pub fn consumed(&self) -> usize {
        self.consumed
    }

    pub fn required(&self) -> usize {
        self.required
    }

    fn with_consumed(token: TokenType<'a>, consumed: usize) -> Self {
        Self {
            token,
            consumed,
            ..Default::default()
        }
    }

    fn with_required(required: usize) -> Self {
        Self {
            token: TokenType::None,
            required,
            ..Default::default()
        }
    }

    fn new(token: TokenType<'a>, consumed: usize, required: usize) -> Self {
        Self {
            token,
            consumed,
            required,
        }
    }

    fn add_offset(self, offset: usize) -> Self {
        Self {
            consumed: self.consumed + offset,
            ..self
        }
    }
}

enum ScanResult<'a> {
    Yield(Token<'a>),
    Scan(usize),
}
use ScanResult::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum State {
    Ground,
    Esc,
    EscIntermediates,
    String,
    DcsEntry,
    CsiEntry,
    CsiParams,
    CsiIntermediates,
    CsiIgnore,
    DcsParams,
    DcsIntermediates,
    DcsIgnore,
}
use State::*;

impl Default for State {
    fn default() -> Self {
        Self::Ground
    }
}

#[derive(Debug, Default)]
pub struct Scanner {
    param: Option<u16>,
    state: State,
}

macro_rules! anywhere {
    ($s: expr, $buffer: expr, $i: expr) => {
        match $s.anywhere(&$buffer[$i..]) {
            Scan(0) => {}
            Scan(n) => return Scan(n + $i),
            Yield(t) => return Yield(t.add_offset($i)),
        }
    };
    ($s: expr, $buffer: expr) => {
        anywhere!($s, $buffer, 0)
    };
}

#[cfg(release)]
macro_rules! debug_panic {
    ($fallback: expr, $_message: expr) => {
        $fallback
    };
}

#[cfg(not(release))]
macro_rules! debug_panic {
    ($_fallback: expr, $message: expr) => {
        unreachable!($message)
    };
}

impl Scanner {
    fn switch_to(&mut self, state: State) {
        dbg!(&state);
        self.state = state;
        match self.state {
            CsiParams | DcsParams => self.param = Some(0),
            _ => {}
        }
    }

    pub fn next<'a>(&mut self, buffer: &'a [u8]) -> Token<'a> {
        let mut i = 0;
        while i < buffer.len() {
            match self.next_token(&buffer[i..]) {
                Yield(t) => return t.add_offset(i),
                Scan(0) => break,
                Scan(n) => i += n,
            }
        }
        Token::new(None, i, 1)
    }

    fn anywhere<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        match buffer {
            // Anywhere -> Ground
            [0x18 | 0x1A | 0x80..=0x8F | 0x91..=0x97 | 0x99, ..] => {
                self.switch_to(Ground);
                Yield(Token::with_consumed(Execute(buffer[0]), 1))
            }
            // Anywhere -> SOS, OSC, PM, or APC
            [0x1B, 0x58 | 0x5D..=0x5F, ..] | [0x98 | 0x9D..=0x9F, ..] => self.start_string(buffer),
            // Anywhere -> DCS Entry
            [0x1B, 0x50, ..] | [0x90, ..] => {
                self.switch_to(DcsEntry);
                let consume = if buffer[0] == 0x1B { 2 } else { 1 };
                Yield(Token::with_consumed(DcsStart, consume))
            }
            // Anywhere -> CSI Entry
            [0x1B, 0x5B, ..] | [0x9B, ..] => {
                self.switch_to(CsiEntry);
                let consume = if buffer[0] == 0x1B { 2 } else { 1 };
                Yield(Token::with_consumed(CsiStart, consume))
            }
            // Anywhere -> SCI
            [0x1B, 0x5A] | [0x9A] => Yield(Token::with_required(1)),
            [0x1B, 0x5A, 0x08..=0x0D | 0x20..=0x7E, ..] | [0x9A, 0x08..=0x0D | 0x20..=0x7E, ..] => {
                let index = if buffer[0] == 0x1B { 2 } else { 1 };
                Yield(Token::with_consumed(Sci(buffer[index]), index + 1))
            }
            [0x1B, 0x5A, ..] | [0x9A, ..] => {
                let consume = if buffer[0] == 0x1B { 2 } else { 1 };
                Yield(Token::with_consumed(None, consume))
            }
            // Anywhere -> Escape
            [0x1B] => Yield(Token::new(TokenType::None, 0, 1)),
            [0x1B, 0x5C, ..] if self.state == String => Scan(0),
            [0x1B, ..] => {
                self.switch_to(Esc);
                Yield(Token::with_consumed(EscStart, 1))
            }
            _ => Scan(0),
        }
    }

    fn update_param(&mut self, c: u8) {
        if let Some(param) = self.param.as_mut() {
            if *param > (PARAM_MAX / 10) {
                return;
            }
            *param *= 10;
            *param += (c - b'0') as u16;
            if *param > PARAM_MAX {
                *param = PARAM_MAX;
            }
        }
    }

    fn create_param(&mut self, replacement: Option<u16>) -> Option<ParamValue> {
        let param = self.param.map(ParamValue::from_u16_lossy);
        self.param = replacement;
        param
    }

    fn apply_escape(buffer: &[u8]) -> (u8, usize) {
        let detect_9 = (buffer[0] & 0x80) >> 7; // For when the buffer contains 0x1B, 0x5.. or 0x9..
        let detect_5 = (buffer[0] & 0x40) >> 6; // For when the buffer contains 0x5..
        let index = 1 - (detect_9 | detect_5) as usize;
        let ty = (buffer[index] | 0x80) & !0x40;
        (ty, index + 1)
    }

    fn start_string<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        let (ty, consumed) = Self::apply_escape(buffer);
        let ty = match ty {
            0x98 => SosStart,
            0x9D => OscStart,
            0x9E => PmStart,
            0x9F => ApcStart,
            v => unreachable!("unexpected string type: {:02x}", v),
        };
        self.switch_to(String);
        Yield(Token::with_consumed(ty, consumed))
    }

    fn next_token<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        anywhere!(self, buffer);
        match self.state {
            Ground => self.next_ground(buffer),
            Esc => self.next_esc(buffer),
            EscIntermediates => self.next_esc_intermediates(buffer),
            String => self.next_string(buffer),
            DcsEntry => self.next_dcs_entry(buffer),
            DcsParams => self.next_dcs_params(buffer),
            DcsIntermediates => self.next_dcs_intermediates(buffer),
            DcsIgnore => self.next_dcs_ignore(buffer),
            CsiEntry => self.next_csi_entry(buffer),
            CsiParams => self.next_csi_params(buffer),
            CsiIntermediates => self.next_csi_intermediates(buffer),
            CsiIgnore => self.next_csi_ignore(buffer),
        }
    }

    fn next_ground<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        let count = buffer
            .iter()
            .take_while(|x| matches!(x, 0x20..=0x7F))
            .count();

        if count != 0 {
            return Yield(Token::with_consumed(Print(&buffer[..count]), count));
        }

        match buffer {
            [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..] => {
                Yield(Token::with_consumed(Execute(buffer[0]), 1))
            }
            [other, ..] => debug_panic!(Scan(1), "unexpected ground byte {other:02x}"),
            [] => Scan(0),
        }
    }

    fn next_esc<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        // Assume that the buffer starts with 0x1B
        let count = buffer.iter().take_while(|x| matches!(x, 0x7F)).count();
        if count != 0 {
            return Scan(count);
        }

        match buffer {
            [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..] => {
                Yield(Token::with_consumed(Execute(buffer[0]), 1))
            }
            [0x5B, ..] => {
                self.switch_to(CsiEntry);
                Yield(Token::with_consumed(CsiStart, 1))
            }
            [0x50, ..] => {
                self.switch_to(DcsEntry);
                Yield(Token::with_consumed(DcsStart, 1))
            }
            [0x30..=0x4F | 0x51..=0x57 | 0x59 | 0x5C | 0x60..=0x7E] => {
                self.switch_to(Ground);
                Yield(Token::with_consumed(Dispatch(buffer[0]), 1))
            }
            [0x20..=0x2F, ..] => {
                self.switch_to(EscIntermediates);
                Yield(Token::with_consumed(Collect(buffer[0]), 1))
            }
            [0x58 | 0x5D..=0x5F, ..] => self.start_string(buffer),
            [other, ..] => debug_panic!(Scan(1), "unexpected escape byte {other:02x}"),
            [] => Scan(0),
        }
    }

    fn next_esc_intermediates<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        let count = buffer.iter().take_while(|x| matches!(x, 0x7F)).count();
        if count != 0 {
            return Scan(count);
        }

        match buffer {
            [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..] => {
                Yield(Token::with_consumed(Execute(buffer[0]), 1))
            }
            [0x30..=0x7E, ..] => {
                self.switch_to(Ground);
                Yield(Token::with_consumed(Dispatch(buffer[0]), 1))
            }
            [0x20..=0x2F, ..] => Yield(Token::with_consumed(Collect(buffer[0]), 1)),
            [other, ..] => debug_panic!(Scan(1), "unexpected escape byte {other:02x}"),
            [] => Scan(0),
        }
    }

    fn next_string<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        let count = buffer
            .iter()
            .take_while(|x| matches!(x, 0x00..=0x17 | 0x1C..=0x1F | 0x20..=0x7F | 0x19))
            .count();

        if count == buffer.len() {
            return Scan(0);
        } else if count != 0 {
            return Yield(Token::with_consumed(Put(&buffer[..count]), count));
        }

        match buffer {
            [0x1B, 0x5C, ..] | [0x9C, ..] => {
                let (_, count) = Self::apply_escape(buffer);
                self.switch_to(Ground);
                Yield(Token::with_consumed(EndString, count))
            }
            [other, ..] => debug_panic!(Scan(1), "unexpected string byte {other:02x}"),
            [] => Scan(0),
        }
    }

    fn next_dcs_entry<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        let count = buffer
            .iter()
            .take_while(|x| matches!(x, 0x00..=0x17 | 0x1C..=0x1F | 0x19 | 0x7F))
            .count();
        if count != 0 {
            return Scan(count);
        }

        match buffer {
            [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..] => {
                Yield(Token::with_consumed(Execute(buffer[0]), 1))
            }
            [0x3A | 0x3C..=0x3F, ..] => {
                self.switch_to(DcsParams);
                Yield(Token::with_consumed(Collect(buffer[0]), 1))
            }
            [0x3B | 0x30..=0x39, ..] => {
                self.switch_to(DcsParams);
                self.next_dcs_params(buffer)
            }
            [0x20..=0x2F, ..] => {
                self.switch_to(DcsIntermediates);
                Yield(Token::with_consumed(Collect(buffer[0]), 1))
            }
            [0x40..=0x7E, ..] => {
                self.switch_to(String);
                self.next_string(buffer)
            }
            [other, ..] => debug_panic!(Scan(1), "unexpected dcs entry byte {other:02x}"),
            [] => Scan(0),
        }
    }

    fn next_dcs_params<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        for (i, c) in buffer.iter().enumerate() {
            anywhere!(self, buffer, i);
            match c {
                0x00..=0x17 | 0x19 | 0x1C..=0x1F | 0x7F => {}
                0x3B => {
                    return Yield(Token::with_consumed(
                        Param(
                            self.create_param(Some(0))
                                .unwrap_or(ParamValue::from_u16_lossy(0)),
                        ),
                        i + 1,
                    ));
                }
                0x30..=0x39 => self.update_param(buffer[i]),
                0x20..=0x2F => {
                    if let Some(param) = self.create_param(Option::None) {
                        return Yield(Token::with_consumed(Param(param), i));
                    }
                    self.switch_to(DcsIntermediates);
                    return Yield(Token::with_consumed(Collect(buffer[i]), i + 1));
                }
                0x40..=0x7E => {
                    if let Some(param) = self.create_param(Option::None) {
                        return Yield(Token::with_consumed(Param(param), i));
                    }
                    self.switch_to(String);
                    debug_assert_eq!(i, 0);
                    return self.next_string(&buffer[i..]);
                }
                _ => {
                    self.switch_to(DcsIgnore);
                    return self.next_dcs_ignore(buffer);
                }
            }
        }
        Scan(buffer.len())
    }

    fn next_dcs_intermediates<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        let count = buffer
            .iter()
            .take_while(|x| matches!(x, 0x00..=0x17 | 0x19 | 0x1C..=0x1F | 0x7F))
            .count();
        if count != 0 {
            return Scan(count);
        }

        match buffer {
            [0x40..=0x7E, ..] => {
                self.switch_to(String);
                self.next_string(buffer)
            }
            [0x20..=0x2F, ..] => Yield(Token::with_consumed(Collect(buffer[0]), 1)),
            [] => Scan(0),
            _ => {
                self.switch_to(DcsIgnore);
                self.next_dcs_ignore(buffer)
            }
        }
    }

    fn next_dcs_ignore<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        match buffer {
            [0x1B, 0x5C, ..] | [0x9C, ..] => {
                let (_, count) = Self::apply_escape(buffer);
                self.switch_to(Ground);
                Scan(count)
            }
            [] => Scan(0),
            _ => {
                let count = buffer
                    .iter()
                    .take_while(|x| !matches!(x, 0x9C | 0x1B))
                    .count();
                Scan(count)
            }
        }
    }

    fn next_csi_entry<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        let count = buffer.iter().take_while(|x| matches!(x, 0x7F)).count();
        if count != 0 {
            return Scan(count);
        }

        match buffer {
            [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..] => {
                Yield(Token::with_consumed(Execute(buffer[0]), 1))
            }
            [0x3A | 0x3C..=0x3F, ..] => {
                self.switch_to(CsiParams);
                Yield(Token::with_consumed(Collect(buffer[0]), 1))
            }
            [0x3B | 0x30..=0x39, ..] => {
                self.switch_to(CsiParams);
                self.next_csi_params(buffer)
            }
            [0x20..=0x2F, ..] => {
                self.switch_to(CsiIntermediates);
                Yield(Token::with_consumed(Collect(buffer[0]), 1))
            }
            [0x40..=0x7E, ..] => {
                self.switch_to(Ground);
                Yield(Token::with_consumed(Dispatch(buffer[0]), 1))
            }
            [other, ..] => debug_panic!(Scan(1), "unexpected csi entry byte {other:02x}"),
            [] => Scan(0),
        }
    }

    fn next_csi_params<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        for (i, c) in buffer.iter().enumerate() {
            anywhere!(self, buffer, i);
            match c {
                0x00..=0x17 | 0x19 | 0x1C..=0x1F => {
                    return Yield(Token::with_consumed(Execute(buffer[i]), i + 1));
                }
                0x7F => {}
                0x3B => {
                    return Yield(Token::with_consumed(
                        Param(
                            self.create_param(Some(0))
                                .unwrap_or(ParamValue::from_u16_lossy(0)),
                        ),
                        i + 1,
                    ));
                }
                0x30..=0x39 => self.update_param(buffer[i]),
                0x20..=0x2F => {
                    if let Some(param) = self.create_param(Option::None) {
                        return Yield(Token::with_consumed(Param(param), i));
                    }
                    self.switch_to(CsiIntermediates);
                    return Yield(Token::with_consumed(Collect(buffer[i]), i + 1));
                }
                0x40..=0x7E => {
                    if let Some(param) = self.create_param(Option::None) {
                        return Yield(Token::with_consumed(Param(param), i));
                    }
                    self.switch_to(Ground);
                    return Yield(Token::with_consumed(Dispatch(buffer[i]), i + 1));
                }
                _ => {
                    self.switch_to(CsiIgnore);
                    return self.next_csi_ignore(buffer);
                }
            }
        }
        Scan(buffer.len())
    }

    fn next_csi_intermediates<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        let count = buffer.iter().take_while(|x| matches!(x, 0x7F)).count();
        if count != 0 {
            return Scan(count);
        }

        match buffer {
            [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..] => {
                Yield(Token::with_consumed(Execute(buffer[0]), 1))
            }
            [0x40..=0x7E, ..] => {
                self.switch_to(Ground);
                Yield(Token::with_consumed(Dispatch(buffer[0]), 1))
            }
            [0x20..=0x2F, ..] => Yield(Token::with_consumed(Collect(buffer[0]), 1)),
            _ => {
                self.switch_to(CsiIgnore);
                self.next_csi_ignore(buffer)
            }
        }
    }

    fn next_csi_ignore<'a>(&mut self, buffer: &'a [u8]) -> ScanResult<'a> {
        for (i, c) in buffer.iter().enumerate() {
            anywhere!(self, buffer, i);
            match c {
                0x00..=0x17 | 0x19 | 0x1C..=0x1F => {
                    return Yield(Token::with_consumed(Execute(buffer[i]), i + 1))
                }
                0x40..=0x7E => {
                    self.switch_to(Ground);
                    return Yield(Token::with_consumed(None, i + 1));
                }
                _ => {}
            }
        }
        Scan(buffer.len())
    }
}

#[cfg(test)]
mod tests {
    use yare::parameterized;

    use super::*;

    const PARAM_0: ParamValue = ParamValue::from_u16_lossy(0);
    const PARAM_100: ParamValue = ParamValue::from_u16_lossy(100);
    const PARAM_1000: ParamValue = ParamValue::from_u16_lossy(1000);
    const PARAM_9999: ParamValue = ParamValue::from_u16_lossy(9999);
    const PARAM_50: ParamValue = ParamValue::from_u16_lossy(50);

    fn run_with_window_length<'a>(
        scanner: &mut Scanner,
        buffer: &'a [u8],
        expected: &[TokenType<'a>],
        s: usize,
    ) {
        let s = if s == 0 { buffer.len() } else { s };

        println!("window length {s}");
        let mut ri = 0;
        let mut start = 0;
        let mut end = (start + s).min(buffer.len());
        while start < buffer.len() {
            let val = scanner.next(&buffer[start..end]);
            if !matches!(val.token, None) {
                assert_eq!(expected[ri], val.token, "token at {ri} should match");
                ri += 1;
            } else if val.required == 0 && val.consumed == 0 {
                break;
            }

            start += val.consumed;
            if val.required != 0 || end <= start {
                if val.consumed == 0 {
                    assert_ne!(
                        buffer.len(),
                        end,
                        "buffer should not be completely consumed {:?}",
                        expected[ri]
                    );
                }
                end = (end + s).max(start + val.required);
                end = end.min(buffer.len());
            }
        }

        assert_eq!(
            expected.len(),
            ri,
            "all tokens should be generated {:?}",
            expected[ri]
        );
        assert_eq!(buffer.len(), start, "the entire buffer should be consumed");
    }

    fn run<'a>(buffer: &'a [u8], expected: &[TokenType<'a>]) {
        let mut scanner = Scanner::default();
        for s in 1..=5 {
            run_with_window_length(&mut scanner, buffer, expected, s);
        }
    }

    #[test]
    pub fn scanner_next_anywhere_to_ground() {
        run(
            b"\x18\x85\x96\x99",
            &[Execute(0x18), Execute(0x85), Execute(0x96), Execute(0x99)],
        );
    }

    #[test]
    pub fn scanner_next_ground() {
        run(
            b"\x18\x85\x96\x99",
            &[Execute(0x18), Execute(0x85), Execute(0x96), Execute(0x99)],
        );
        run_with_window_length(
            &mut Scanner::default(),
            b"\x10\x19\x1DHello\x10World",
            &[
                Execute(0x10),
                Execute(0x19),
                Execute(0x1D),
                Print(b"Hello"),
                Execute(0x10),
                Print(b"World"),
            ],
            0,
        );
    }

    #[test]
    pub fn scanner_next_esc() {
        run(
            b"\x1B\x7F\x7F\x10\x19\x1D",
            &[EscStart, Execute(0x10), Execute(0x19), Execute(0x1D)],
        );
    }

    #[test]
    pub fn scanner_next_esc_immediate_dispatch() {
        run(
            b"\x1B\x10\x19\x1D\x35",
            &[
                EscStart,
                Execute(0x10),
                Execute(0x19),
                Execute(0x1D),
                Dispatch(0x35),
            ],
        );
    }

    #[test]
    pub fn scanner_next_esc_intermediates_dispatch() {
        run(
            b"\x1B\x10\x19/\x1D+\x7F\x7FA",
            &[
                EscStart,
                Execute(0x10),
                Execute(0x19),
                Collect(b'/'),
                Execute(0x1D),
                Collect(b'+'),
                Dispatch(b'A'),
            ],
        );
    }

    #[parameterized(
        dsc = { b"\x1B\x00\x50", DcsStart },
        sos = { b"\x1B\x00\x58", SosStart },
        csi = { b"\x1B\x00\x5B", CsiStart },
        osc = { b"\x1B\x00\x5D", OscStart },
        pm = { b"\x1B\x00\x5E", PmStart },
        apc = { b"\x1B\x00\x5F", ApcStart },
    )]
    pub fn scanner_escape_codes(buffer: &[u8], ty: TokenType) {
        run(buffer, &[EscStart, Execute(0x00), ty]);
    }

    #[parameterized(
        osc_7bit = { b"\x1B\x5D\x00\x19\x1D1000ABC\x1B\x5C", OscStart },
        osc_8bit = { b"\x9D\x00\x19\x1D1000ABC\x9C", OscStart },
        pm_7bit = { b"\x1B\x5E\x00\x19\x1D1000ABC\x1B\x5C", PmStart },
        pm_8bit = { b"\x9E\x00\x19\x1D1000ABC\x9C", PmStart },
        sos_7bit = { b"\x1B\x58\x00\x19\x1D1000ABC\x1B\x5C", SosStart },
        sos_8bit = { b"\x98\x00\x19\x1D1000ABC\x9C", SosStart },
        apc_7bit = { b"\x1B\x5F\x00\x19\x1D1000ABC\x1B\x5C", ApcStart },
        apc_8bit = { b"\x9F\x00\x19\x1D1000ABC\x9C", ApcStart }
    )]
    pub fn scanner_next_string(buffer: &[u8], ty: TokenType) {
        run(buffer, &[ty, Put(b"\x00\x19\x1D1000ABC"), EndString]);
    }

    #[parameterized(
        csi_7bit = { b"\x1B\x5B\x00\x19\x1D\x7FA" },
        csi_8bit = { b"\x9B\x00\x19\x1D\x7FA" },
    )]
    pub fn scanner_next_csi_immediate_dispatch(buffer: &[u8]) {
        run(
            buffer,
            &[
                CsiStart,
                Execute(0x00),
                Execute(0x19),
                Execute(0x1D),
                Dispatch(b'A'),
            ],
        );
    }

    #[parameterized(
        sci_7bit = { b"\x1B\x5A\x0D" },
        sci_8bit = { b"\x9A\x0D" },
    )]
    pub fn scanner_next_sci(buffer: &[u8]) {
        run(buffer, &[Sci(0x0D)]);
    }

    #[parameterized(
        sci_7bit = { b"\x1B\x5A\x19" },
        sci_8bit = { b"\x9A\x19" },
    )]
    pub fn scanner_invalid_sci(buffer: &[u8]) {
        run(buffer, &[Execute(0x19)]);
    }

    #[test]
    pub fn scanner_next_csi_params_dispatch() {
        run(
            b"\x9B\x00100\x190\x1D;99999\x7F9;50A",
            &[
                CsiStart,
                Execute(0x00),
                Execute(0x19),
                Execute(0x1D),
                Param(PARAM_1000),
                Param(PARAM_9999),
                Param(PARAM_50),
                Dispatch(b'A'),
            ],
        );
    }

    #[test]
    pub fn scanner_next_csi_params_ignore() {
        run(
            b"\x9B100?100\x00\x19\x1D;1;/A",
            &[CsiStart, Execute(0x00), Execute(0x19), Execute(0x1D)],
        );
    }

    #[test]
    pub fn scanner_next_csi_intermediate_dispatch() {
        run(
            b"\x9B\x00100/\x1D\x7FA",
            &[
                CsiStart,
                Execute(0x00),
                Param(PARAM_100),
                Collect(b'/'),
                Execute(0x1D),
                Dispatch(b'A'),
            ],
        );
    }

    #[test]
    pub fn scanner_next_csi_start_char_dispatch() {
        run(
            b"\x9B?\x00100/\x1D\x7FA",
            &[
                CsiStart,
                Collect(b'?'),
                Execute(0x00),
                Param(PARAM_100),
                Collect(b'/'),
                Execute(0x1D),
                Dispatch(b'A'),
            ],
        );
    }

    #[test]
    pub fn scanner_next_csi_immediate_intermediates_dispatch() {
        run(
            b"\x9B\x00/A",
            &[CsiStart, Execute(0x00), Collect(b'/'), Dispatch(b'A')],
        );
    }

    #[test]
    pub fn scanner_next_csi_immediate_intermediates_ignore() {
        run(
            b"\x9B\x00/&\xA0A",
            &[CsiStart, Execute(0x00), Collect(b'/'), Collect(b'&')],
        );
    }

    #[parameterized(
        dcs_7bit = { b"\x1B\x50\x00\x19\x1D\x7FHello World\x1B\x5C" },
        dcs_8bit = { b"\x90\x00\x19\x1D\x7FHello World\x9C" },
    )]
    pub fn scanner_next_dcs_immediate_terminate(buffer: &[u8]) {
        run(buffer, &[DcsStart, Put(b"Hello World"), EndString]);
    }

    #[test]
    pub fn scanner_next_dcs_params_dispatch() {
        run(
            b"\x90\x00;100\x190\x1D;99999\x7F9;50Hello\x15\x19\x1D World\x9C",
            &[
                DcsStart,
                Param(PARAM_0),
                Param(PARAM_1000),
                Param(PARAM_9999),
                Param(PARAM_50),
                Put(b"Hello\x15\x19\x1D World"),
                EndString,
            ],
        );
    }

    #[test]
    pub fn scanner_next_dcs_params_ignore() {
        run(b"\x90100?100\x00\x19\x1D;1;/Hello World\x9C", &[DcsStart]);
    }

    #[test]
    pub fn scanner_next_dcs_intermediate_dispatch() {
        run(
            b"\x90\x00100/\x1D\x7FFoobar\x9C",
            &[
                DcsStart,
                Param(PARAM_100),
                Collect(b'/'),
                Put(b"Foobar"),
                EndString,
            ],
        );
    }

    #[test]
    pub fn scanner_next_dcs_start_char_dispatch() {
        run(
            b"\x90?\x00100/\x1D\x7FHello\x9C",
            &[
                DcsStart,
                Collect(b'?'),
                Param(PARAM_100),
                Collect(b'/'),
                Put(b"Hello"),
                EndString,
            ],
        );
    }

    #[test]
    pub fn scanner_next_dcs_immediate_intermediates_dispatch() {
        run(
            b"\x90\x00/Hello\x9C",
            &[DcsStart, Collect(b'/'), Put(b"Hello"), EndString],
        );
    }

    #[test]
    pub fn scanner_next_dcs_immediate_intermediates_ignore() {
        run(
            b"\x90\x00/&\xA0A\x9C",
            &[DcsStart, Collect(b'/'), Collect(b'&')],
        );
    }
}
